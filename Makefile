.PHONY: help setup test clean

help:
	@echo "Available commands:"
	@echo "  setup:    Build for development"
	@echo "  test:     Test module"
	@echo "  clean:    Clean up temporary files"

setup:
	python3 setup.py develop

test:
	python -m pytest --verbose --color=yes --junitxml=test_results tests

sdist:
	python3 setup.py sdist

clean:
	rm -rf MANIFEST
	rm -rf docs
	rm -rf .eggs .cache .coverage htmlcov dist
	rm -rf *.egg-info
	find . -type d -name '__pycache__' -exec rm -rf '{}' \; || exit 0
	find . -name '*.pyc' -exec rm -rf '{}' \;

