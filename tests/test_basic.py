"""
Basic testing of functionality
"""

import boto3
from unittest.mock import Mock

from boto3_extensions import arn_patch_boto3

arn_patch_boto3()

table_data = {
    'TableArn':
    'arn:aws:dynamodb:ap-southeast-2:123456789010:table/DynamoDBCrossRegionReplication',
    'TableName': 'DynamoDBCrossRegionReplication',
}

instance_data = {'AmiLaunchIndex': 0,
                 'Architecture': 'x86_64',
                 'InstanceId': 'i-f0bc5eee',
                 'InstanceType': 'm4.large'}

user_data = {'Arn': 'arn:aws:iam::123456789010:user/test-user',
             'Path': '/',
             'UserId': 'XXXXXXXXXXXXXXXXXXXXX',
             'UserName': 'test-user'}

route53_hosted_zone_data = {
        'CallerReference': '48867CAA-35D1-B83C-B07D-488F6E78C50E',
        'Config': {'PrivateZone': True},
        'Id': '/hostedzone/ZMIA3ZCSHT2KA',
        'Name': 'internal.webteam.com.',
        'ResourceRecordSetCount': 3
}

session = boto3.Session(region_name='us-east-1')


def test_patching_session():
    iam = session.resource('iam')
    assert (hasattr(iam.meta, 'session'))
    assert (type(iam.meta.session) == boto3.Session)
    assert (iam.meta.session.region_name == 'us-east-1')


def test_datapath_arn_supprt():
    dynamo = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamo.Table('test-table')
    table.meta.data = table_data
    arn = table.arn
    assert(arn == table_data['TableArn'])


def test_format_string_arn_support():
    ec2 = session.resource('ec2')
    instance = ec2.Instance('i-f0bc5eee')
    instance.meta.data = instance_data
    # set account id so we don't need to login
    instance.meta.session = Mock()
    instance.meta.session.account_id = '123456789010'
    instance.meta.session.region_name = 'us-east-1'
    arn = instance.arn
    actual_arn = 'arn:aws:ec2:us-east-1:123456789010:instance/i-f0bc5eee'
    assert(arn == actual_arn)


# Commented out until route53 resource is merged into boto3
#def test_format_string_with_replace_arn_support():
#    r53 = session.resource('route53')
#    hosted_zone = r53.HostedZone('/hostedzone/ZMIA3ZCSHT2KA')
#    hosted_zone.meta.data = route53_hosted_zone_data
#    # set account id so we don't need to login
#    hosted_zone.meta.session = Mock()
#    hosted_zone.meta.session.account_id = '123456789010'
#    hosted_zone.meta.session.region_name = 'us-east-1'
#    arn = hosted_zone.arn
#    actual_arn = 'arn:aws:route53:::hostedzone/ZMIA3ZCSHT2KA'
#    assert(arn == actual_arn)


def test_normal_arn_support():
    iam = session.resource('iam')
    user = iam.User('test-user')
    user.meta.data = user_data
    assert(user.arn == user_data['Arn'])
